from PyQt5 import QtCore, QtGui, QtWidgets


class Spoiler(QtWidgets.QWidget):
    def __init__(self, collapseDirection = "right", parent=None, title='', animationDuration=300):
        """
        References:
            # Adapted from c++ version
            http://stackoverflow.com/questions/32476006/how-to-make-an-expandable-collapsable-section-widget-in-qt
        """
        super(Spoiler, self).__init__(parent=parent)

        self.collapseDirection = collapseDirection
        self.animationDuration = animationDuration
        self.toggleAnimation = QtCore.QParallelAnimationGroup()
        self.toggleButton = QtWidgets.QToolButton()
        self.mainLayout = QtWidgets.QHBoxLayout()
        self.mainLayout.setContentsMargins(0,0,0,0)

        toggleButton = self.toggleButton
        toggleButton.setStyleSheet("QToolButton { border: none; }")
        toggleButton.setToolButtonStyle(QtCore.Qt.ToolButtonTextBesideIcon)
        if (collapseDirection == "right"):
            toggleButton.setArrowType(QtCore.Qt.RightArrow)
        elif (collapseDirection == "left"):
            toggleButton.setArrowType(QtCore.Qt.LeftArrow)

        toggleButton.setText(str(title))
        toggleButton.setCheckable(True)
        toggleButton.setChecked(True)
        toggleButton.setFixedWidth(25)
        toggleButton.setMinimumWidth(25)

        # let the entire widget grow and shrink with its content
        toggleAnimation = self.toggleAnimation
        toggleAnimation.addAnimation(QtCore.QPropertyAnimation(self, b"size"))
        #  toggleAnimation.addAnimation(QtCore.QPropertyAnimation(self, b"maximumWidth"))
        #  toggleAnimation.addAnimation(QtCore.QPropertyAnimation(self, b"minimumWidth"))
        # don't waste space
        mainLayout = self.mainLayout
        mainLayout.setContentsMargins(0, 0, 0, 0)
        if self.collapseDirection == "right":
            mainLayout.addWidget(self.toggleButton)

        #  self.mainLayout.addStretch()
        self.setLayout(self.mainLayout)
        self.contentWidget = None

        def start_animation(checked):
            if self.collapseDirection == "right":
                arrow_type = QtCore.Qt.RightArrow if checked else QtCore.Qt.LeftArrow

            elif self.collapseDirection == "left":
                arrow_type = QtCore.Qt.LeftArrow if checked else QtCore.Qt.RightArrow
                #  direction = QtCore.QAbstractAnimation.Backward if checked else QtCore.QAbstractAnimation.Forward

            direction = QtCore.QAbstractAnimation.Forward if checked else QtCore.QAbstractAnimation.Backward
            toggleButton.setArrowType(arrow_type)
            self.toggleAnimation.setDirection(direction)

            contentWidth = self.contentWidget.sizeHint().width()
            contentHeight = self.contentWidget.height()
            contentAnimation = self.toggleAnimation.animationAt(0) #self.toggleAnimation.animationCount() - 1)
            contentAnimation.setDuration(self.animationDuration)
            contentAnimation.setStartValue(QtCore.QSize(self.toggleButton.width(), contentHeight))
            contentAnimation.setEndValue(QtCore.QSize(contentWidth+self.toggleButton.width(), contentHeight))

            self.toggleAnimation.start()
            self.contentWidget.show() if checked else self.contentWidget.hide()

        self.toggleButton.clicked.connect(start_animation)

    def setContentLayout(self, contentWidget):

        self.contentWidget = contentWidget
        self.mainLayout.addWidget(contentWidget)
        if self.collapseDirection == "left":
           self. mainLayout.addWidget(self.toggleButton)

        contentWidget.setMinimumWidth(0)
        collapsedWidth = self.toggleButton.width()#self.sizeHint().width() - contentWidget.maximumWidth()
        contentWidth = contentWidget.sizeHint().width()
        contentHeight = contentWidget.height()
        for i in range(self.toggleAnimation.animationCount()-1):
            spoilerAnimation = self.toggleAnimation.animationAt(i)
            spoilerAnimation.setDuration(self.animationDuration)
            spoilerAnimation.setStartValue(QtCore.QSize(collapsedWidth, contentHeight))
            spoilerAnimation.setEndValue(QtCore.QSize(collapsedWidth + contentWidth, contentHeight))

        contentAnimation = self.toggleAnimation.animationAt(0) #self.toggleAnimation.animationCount() - 1)
        contentAnimation.setDuration(self.animationDuration)
        contentAnimation.setStartValue(QtCore.QSize(self.toggleButton.width(), contentHeight))
        contentAnimation.setEndValue(QtCore.QSize(contentWidth+self.toggleButton.width(), contentHeight))
